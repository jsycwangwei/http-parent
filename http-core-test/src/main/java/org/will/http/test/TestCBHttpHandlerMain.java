package org.will.http.test;

import org.will.http.CBHttpBootStrap;
import org.will.http.CBHttpHandlerManager;
import org.will.http.config.CBHttpHandlerConfig;
import org.will.http.domain.CBHttpResult;

import java.security.KeyManagementException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;

/**
 * @Author wangwei-ww
 * @Date 2017/3/29 9:52
 * @Comment
 */
public class TestCBHttpHandlerMain {

    public static void main(String []args)throws NoSuchAlgorithmException, KeyStoreException, KeyManagementException, InterruptedException {
        CBHttpHandlerConfig config = new CBHttpHandlerConfig();
        config.setAutoMeasure(true);
        config.setMeasureInterval(10000);
        config.setProxyConfigs("192.168.16.189:8080:wangwei-ww:00000000,192.168.16.187:8080:wangwei-ww:00000000,");
        CBHttpBootStrap bootStrap = new CBHttpBootStrap(config);
        for (int i = 1; i < 30; i++) {
            CBHttpResult result = CBHttpHandlerManager.getHttpHandler().get("https://api.doba.com", null);
            System.out.println(result);
            System.out.println("Current Proxy " + CBHttpHandlerManager.getHttpHandler().getHttpConfig().getProxy());
            Thread.currentThread().sleep(5000);
        }
    }
}
