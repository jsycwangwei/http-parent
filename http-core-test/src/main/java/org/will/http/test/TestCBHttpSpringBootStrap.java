package org.will.http.test;

import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 * @Author wangwei-ww
 * @Date 2017/3/30 11:35
 * @Comment 
 */
public class TestCBHttpSpringBootStrap {

    @SuppressWarnings("resource")
    public static void main(String[] args) throws Exception {
        new ClassPathXmlApplicationContext("http.xml");
        System.in.read();
        System.exit(0);
    }
}
