package org.will.http;

import org.will.http.service.ICBHttpHandler;
import org.will.http.strategy.ISelectProxyStragegy;

import java.util.ArrayList;
import java.util.List;

/**
 * @Author wangwei-ww
 * @Date 2017/3/29 19:45
 * @Comment 管理所有HttpHandler
 */
public class CBHttpHandlerManager {
    private static List<ICBHttpHandler> httpHandlers = new ArrayList<>();
    private static ISelectProxyStragegy selectProxyStategy = null;

    public CBHttpHandlerManager(){}

    public static List<ICBHttpHandler> getAllHttpHandler(){
        return httpHandlers;
    }

    public static void addHttpHandler(ICBHttpHandler handler){
        if(!httpHandlers.contains(handler)){
            httpHandlers.add(handler);
        }
    }

    public static ICBHttpHandler getHttpHandler(){
        return getSelectProxyStategy().select(getAllHttpHandler());
    }

    private static ISelectProxyStragegy getSelectProxyStategy() {
        if(selectProxyStategy == null){
            selectProxyStategy = ISelectProxyStragegy.FASTEST;
        }
        return selectProxyStategy;
    }

    public void setSelectProxyStategy(ISelectProxyStragegy selectProxyStategy) {
        CBHttpHandlerManager.selectProxyStategy = selectProxyStategy;
    }
}
