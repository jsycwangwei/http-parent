package org.will.http.strategy;

import org.will.http.service.ICBHttpHandler;

import java.util.List;
import java.util.Random;

/**
 * @Author wangwei-ww
 * @Date 2017/3/29 21:00
 * @Comment
 */
public class SelectRandomHttpHandlerStrategy implements ISelectProxyStragegy {

    Random random = new Random();

    @Override
    public ICBHttpHandler select(List<ICBHttpHandler> handlers) {
        if (handlers.size() == 1) {
            return handlers.get(0);
        }
        return handlers.get(random.nextInt(handlers.size()));
    }
}
