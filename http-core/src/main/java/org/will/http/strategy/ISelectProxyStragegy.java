package org.will.http.strategy;

import org.will.http.service.ICBHttpHandler;

import java.util.List;

/**
 * @Author wangwei-ww
 * @Date 2017/3/29 20:56
 * @Comment HTTP代理选择策略
 */
public interface ISelectProxyStragegy {
    ISelectProxyStragegy FASTEST = new SelectFastHttpHandlerStrategy();

    ICBHttpHandler select(List<ICBHttpHandler> handlers);
}
