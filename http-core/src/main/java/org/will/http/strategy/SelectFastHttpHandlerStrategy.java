package org.will.http.strategy;

import org.will.http.service.ICBHttpHandler;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;

/**
 * @Author wangwei-ww
 * @Date 2017/3/29 20:58
 * @Comment
 */
public class SelectFastHttpHandlerStrategy implements ISelectProxyStragegy {

    private final static Comparator<ICBHttpHandler> COMPARATOR = new Comparator<ICBHttpHandler>() {
        @Override
        public int compare(ICBHttpHandler o1, ICBHttpHandler o2) {
            float result = o1.getMeasureCost() - o2.getMeasureCost();
            if (result > 0) {
                return 1;
            } else if (result < 0) {
                return -1;
            } else {
                return 0;
            }
        }
    };


    @Override
    public ICBHttpHandler select(List<ICBHttpHandler> handlers) {
        if (handlers.size() > 1) {
            Collections.sort(handlers, COMPARATOR);
        }
        return handlers.get(0);
    }
}
