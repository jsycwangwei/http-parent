package org.will.http;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @Author wangwei-ww
 * @Date 2017/3/30 11:02
 * @Comment 
 */
public class CBHttpLogFactory {
    private static Logger logger = LoggerFactory.getLogger("cbhttp");

    public static Logger getLogger(){
        return logger;
    }
}
