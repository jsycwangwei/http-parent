package org.will.http.util;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @Author wangwei-ww
 * @Date 2017/3/28 11:28
 * @Comment 
 */
public class CBHttpUtil {

    private static Pattern pattern = Pattern.compile(".*charset=([^;]*).*");

    public static String getCharSet(String content) {
        Matcher matcher = pattern.matcher(content);
        if (matcher.find()) {
            return matcher.group(1);
        }
        else {
            return "";
        }
    }
}
