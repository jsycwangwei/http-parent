CBHttp
========
[Version] 1.0
[JDK] 1.7+
[HttpClient] 4.3+

## 功能
一个基于HttpClient封装的Http组件,同时提供以下特性：
* GET & POST 参数、头信息及响应封装
* 请求连接池管理
* 多代理配置管理
* 代理选择策略：最快代理；随机代理；加权策略
* 支持schema包括http, https(TLS协议)

## 安装
### Maven
在项目的pom.xml的dependencies中加入以下内容:
####xml
<dependency>
    <groupId>com.focustech.cb</groupId>
    <artifactId>http-core</artifactId>
    <version>1.0-SNAPSHOT</version>
</dependency>
####

### Java启动配置
#### 在项目中加入Proxy配置器
CBHttpHandlerConfig config = new CBHttpHandlerConfig();
config.setAutoMeasure(true);
config.setMeasureInterval(10000);
config.setProxyConfigs("192.168.X.X:8080:AA:BB,192.168.X.X:8080:CC:DD,");
#### 在项目中加入Http启动器
CBHttpBootStrap bootStrap = new CBHttpBootStrap(config);
####

### Spring启动配置
<bean id="cbHttpHandlerConfig" class="CBHttpHandlerConfig">
   <property name="proxyConfigs" value="192.168.X.X:8080:AA:BB,192.168.X.X:8080:CC:DD," />
   <property name="autoMeasure" value="true" />
   <property name="measureInterval" value="60000" />
</bean>
<bean class="CBHttpBootStrap">
   <constructor-arg>
      <ref bean="cbHttpHandlerConfig"></ref>
   </constructor-arg>
</bean>
####