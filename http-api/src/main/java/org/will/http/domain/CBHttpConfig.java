package org.will.http.domain;

import java.util.ArrayList;
import java.util.List;

/**
 * @Author wangwei-ww
 * @Date 2017/3/27 19:22
 * @Comment 
 */
public class CBHttpConfig {
    private int connectionTimeout = 30000;
    private int readTimeout = 60000;
    private CBHttpProxy proxy;
    private List<CBHttpCredential> credentials = new ArrayList<CBHttpCredential>();
    private int keepaliveTimeout = 30;
    private int maxIdleTime = 30;
    private boolean autoRetry;

    public CBHttpProxy getProxy() {
        return proxy;
    }

    public void setProxy(CBHttpProxy proxy) {
        this.proxy = proxy;
    }

    public void setCredentials(List<CBHttpCredential> credentials) {
        this.credentials = credentials;
    }

    public int getReadTimeout() {
        return readTimeout;
    }

    public void setReadTimeout(int readTimeout) {
        this.readTimeout = readTimeout;
    }

    public int getConnectionTimeout() {
        return connectionTimeout;
    }

    public void setConnectionTimeout(int connectionTimeout) {
        this.connectionTimeout = connectionTimeout;
    }

    public List<CBHttpCredential> getCredentials() {
        return credentials;
    }

    public void addCredentials(CBHttpCredential credential) {
        this.credentials.add(credential);
    }

    public int getKeepaliveTimeout() {
        return keepaliveTimeout;
    }

    public void setKeepaliveTimeout(int keepaliveTimeout) {
        this.keepaliveTimeout = keepaliveTimeout;
    }

    public int getMaxIdleTime() {
        return maxIdleTime;
    }

    public void setMaxIdleTime(int maxIdleTime) {
        this.maxIdleTime = maxIdleTime;
    }

    public boolean isAutoRetry() {
        return autoRetry;
    }

    public void setAutoRetry(boolean autoRetry) {
        this.autoRetry = autoRetry;
    }

}
