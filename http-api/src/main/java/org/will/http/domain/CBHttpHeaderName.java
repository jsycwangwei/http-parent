/*
 * Copyright 2017 Focus Technology, Co., Ltd. All rights reserved.
 */
/**
 * 
 */
package org.will.http.domain;


public enum CBHttpHeaderName {
    SET_COOKIE("set_cookie"), CONTENT_TYPE("content-type"), CONTENT_ENCODING("content-encoding"),
    CONTENT_LENGTH("content-length");
    private String value;

    CBHttpHeaderName(String value) {
        this.setValue(value);
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }
}
