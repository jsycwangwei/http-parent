/*
 * Copyright 2017 Focus Technology, Co., Ltd. All rights reserved.
 */
package org.will.http.domain;

/**
 * @Author wangwei-ww
 * @Date 2017/3/27 9:07
 * @Comment 
 */
public class CBHttpCredential {
    private String host;
    private int port = 80;
    private String username;
    private String password;

    public String getHost() {
        return host;
    }

    public void setHost(String host) {
        this.host = host;
    }

    public int getPort() {
        return port;
    }

    public void setPort(int port) {
        this.port = port;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }
}
