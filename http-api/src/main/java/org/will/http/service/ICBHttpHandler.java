package org.will.http.service;

import org.will.http.domain.CBHttpConfig;
import org.will.http.domain.CBHttpResult;

import java.util.Map;

/**
 * @Author wangwei-ww
 * @Date 2017/3/27 19:14
 * @Comment
 */
public interface ICBHttpHandler {
    CBHttpResult get(String url, Map<String, String> headers);

    CBHttpResult post(String url, Map<String, String> headers, Map<String, Object> params);

    CBHttpResult postStream(String url, Map<String, String> headers, byte[] rawData);

    CBHttpResult postJson(String url, Map<String, String> headers, String json);

    CBHttpResult postMultiPart(String url, Map<String, String> headers, Map<String, Object> params);

    CBHttpConfig getHttpConfig();

    Long getMeasureCost();

    void setMeasureCost(long cost);

    void shutdown();
}
